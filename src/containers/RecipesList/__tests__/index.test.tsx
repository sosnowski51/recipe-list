import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import RecipesListContainer from '../index';

const RecipesList = (): any => {
  return <RecipesListContainer />;
};

it('renders recipes list component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecipesList />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create recipes list component snapshot', () => {
  const component = renderer.create(<RecipesList />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
