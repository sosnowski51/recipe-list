import styled, { StyledComponent } from '@emotion/styled';

import { ITheme } from '../../interfaces/dresscode/ITheme';

type IThemeProps = {
  theme: ITheme;
};

export const Container: StyledComponent<any, IThemeProps, any> = styled.div`
  font-family: ${({theme}) => theme.typography.fontFamily};
  display: flex;
  width: 100%;
  min-height: 100vh;
  justify-content: center;
  align-items: flex-end;
  position: relative;
  flex-wrap: wrap;
  background: linear-gradient(230deg, #a24bcf, #4b79cf, #4bc5cf);
  background-size: 300% 300%;
  ${({theme}) => theme.animations.mainBg}
`;

export const Wrapper: StyledComponent<any, IThemeProps, any> = styled.div`
  display: flex;
  flex-basis: 90%;
  max-width: 90%;
  flex-flow: row wrap;
  border-radius: 3px;
  padding: 15px;
  box-sizing: border-box;
  border: 1px solid ${({theme}) => theme.colors.silver};
  box-shadow: 0 0 50px 0 rgba(0, 0, 0, 0.15);
  background-color: ${({theme}) => theme.colors.white};
  align-self: center;
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    max-width: 70%;
    flex-basis: 70%;
    padding: 20px;
  }
  @media (min-width: ${({theme}) => theme.mediaQueries.desktop}) {
    max-width: 40%;
    flex-basis: 40%;
  }
`;

export const RecipesList = styled.ul<{}>`
  flex-basis: 100%;
  max-width: 100%;
  padding: 0;
  list-style-type: none;
  margin-bottom: 30px;
  font-weight: 100;
  font-size: 20px;
  line-height: 1;
`;

export const AlertInfo: StyledComponent<any, IThemeProps, any> = styled.div`
  margin: 0 0 20px;
  padding: 15px;
  background-color: ${({theme}) => theme.colors.lightRed};
  text-align: center;
  border-radius: 3px;
  border: 1px solid ${({theme}) => theme.colors.lightPink};
  color: ${({theme}) => theme.colors.white};
  font-size: 20px;
  box-sizing: border-box;
  line-height: 1;
  flex-basis: 100%;
  max-width: 100%;
`;

export const ButtonDefault: StyledComponent<any, IThemeProps, any> = styled.button`
  border: 0;
  background-color: ${({theme}) => theme.colors.blue};
  color: ${({theme}) => theme.colors.white};
  padding: 15px 25px;
  border-radius: 3px;
  text-transform: uppercase;
  font-weight: bold;
  line-height: 1;
  cursor: pointer;
  transition: background-color 0.3s ease-out;
  &:hover,
  &:focus {
    background-color: ${({theme}) => theme.colors.turquise};
  }
`;
