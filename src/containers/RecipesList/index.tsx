import React, { Component } from 'react';

import { ThemeProvider } from 'emotion-theming';
import theme from '../../dresscode';
import * as labels from '../../const/labels.json';

import RecipeItem from '../../components/RecipeItem';
import CustomPopup from '../../components/CustomPopup';
import ListTitle from '../../components/ListTitle';
import Footer from '../../components/Footer';
import {
  RecipesList,
  Container,
  Wrapper,
  AlertInfo,
  ButtonDefault,
} from './styledComponents';

import { ILabels } from '../../interfaces/dataStructure/ILabels';
import { IListItem } from '../../interfaces/dataStructure/IListItem';
import { localStorage } from '../../helpers/localStorage';

type IRecipesListState = {
  items: IListItem[];
  showAddItemPopup: boolean;
  showEditItemPopup: boolean;
  itemToUpdate: IListItem;
};

class RecipesListContainer extends Component<{}, IRecipesListState> {
  public state = {
    items: localStorage.getItem('recipesList') || [],
    showAddItemPopup: false,
    showEditItemPopup: false,
    itemToUpdate: {
      name: '',
      details: '',
      id: 0,
    },
  };

  private addToList = (data: any) => {
    this.setState(state => ({
      items: [...state.items, data],
      showAddItemPopup: !state.showAddItemPopup,
    }));
  };

  private editItem = (data: IListItem) => {
    const { items } = this.state;
    const updatedItems = items.map((item: IListItem) =>
      item.id === data.id ? data : item
    );

    this.setState(prevState => ({
      showEditItemPopup: !prevState.showEditItemPopup,
      items: updatedItems,
    }));
  };

  private deleteItem = (id: number) => {
    const { items } = this.state;
    const updatedItems = items.filter((item: IListItem) => item.id !== id);

    this.setState({ items: updatedItems });
  };

  private showEditItemPopup = (data: IListItem = this.state.itemToUpdate) => {
    this.setState(prevState => ({
      showEditItemPopup: !prevState.showEditItemPopup,
      itemToUpdate: data,
    }));
  };

  private showAddItemPopup = () => {
    this.setState(prevState => ({
      showAddItemPopup: !prevState.showAddItemPopup,
    }));
  };

  private saveStateToLocalStorage = () =>
    localStorage.setItem('recipesList', this.state.items);

  public componentDidMount() {
    window.addEventListener('beforeunload', this.saveStateToLocalStorage);
  }

  public componentWillUnmount() {
    window.removeEventListener('beforeunload', this.saveStateToLocalStorage);

    // save if component will be unmounted
    this.saveStateToLocalStorage();
  }

  public render() {
    const {
      items,
      showAddItemPopup,
      showEditItemPopup,
      itemToUpdate,
    } = this.state;

    const {
      mainTitle,
      subtitle,
      description,
      alertInfo,
      addNewButtonLabel,
      credentials,
    }: ILabels = labels;

    return (
      <ThemeProvider theme={theme}>
        <Container>
          <ListTitle
            mainTitle={mainTitle}
            subtitle={subtitle}
            description={description}
          />
          <Wrapper>
            {items.length ? (
              <RecipesList>
                {items.map((item: IListItem) => (
                  <RecipeItem
                    item={item}
                    key={item.id}
                    showEditItemPopup={this.showEditItemPopup}
                    deleteItem={this.deleteItem}
                  />
                ))}
              </RecipesList>
            ) : (
              <AlertInfo>{alertInfo}</AlertInfo>
            )}

            {showAddItemPopup && (
              <CustomPopup
                closePopupMethod={this.showAddItemPopup}
                options={{
                  buttonLabel: 'Add Recipe',
                  headerLabel: 'Add new recipe',
                  method: this.addToList,
                }}
              />
            )}

            {showEditItemPopup && (
              <CustomPopup
                closePopupMethod={this.showEditItemPopup}
                recipeNameValue={itemToUpdate.name}
                recipeDetailsValue={itemToUpdate.details}
                recipeIdNumber={itemToUpdate.id}
                options={{
                  buttonLabel: 'Edit Recipe',
                  headerLabel: 'Edit Recipe: ',
                  method: this.editItem,
                }}
              />
            )}
            <ButtonDefault onClick={this.showAddItemPopup}>
              {addNewButtonLabel}
            </ButtonDefault>
          </Wrapper>
          <Footer>{credentials}</Footer>
        </Container>
      </ThemeProvider>
    );
  }
}

export default RecipesListContainer;
