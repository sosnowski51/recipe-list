const colors = {
    black: '#000',
    white: '#fff',
    lightWhite: '#f1f1f1',
    silver: '#eaeaea',
    lightRed: '#ff6e6e',
    lightPink: '#ffafaf',
    red: '#d82626',
    blue: '#3b8ad5',
    turquise: '#29a2d5',
    darkTurquise: '#5c6ed5',
}

export default colors;