const mediaQueries = {
  mobile: '767px',
  desktop: '1240px',
}

export default mediaQueries;