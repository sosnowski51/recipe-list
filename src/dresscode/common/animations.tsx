const animations = {
  mainBg: `
  -webkit-animation: bg-animation 30s ease infinite;
  -moz-animation: bg-animation 30s ease infinite;
  animation: bg-animation 30s ease infinite;

  @-webkit-keyframes bg-animation {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
  @-moz-keyframes bg-animation {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
  @keyframes bg-animation {
    0% {
      background-position: 0% 50%;
    }
    50% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0% 50%;
    }
  }
  `,
};

export default animations;
