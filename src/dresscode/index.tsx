import colors from './common/colors';
import animations from './common/animations';
import typography from './common/typography';
import mediaQueries from './common/mediaQueries';

const theme = {
  typography,
  mediaQueries,
  colors,
  animations,
};

export default theme;
