import React, { PureComponent, SyntheticEvent } from 'react';

import * as labels from '../../const/labels.json';
import { getTimestampId } from '../../helpers/generators';

import {
  CustomPopupContainer,
  Mask,
  Form,
  FormBody,
  Header,
  ButtonIconClose,
  Label,
  LabelTitle,
  InputText,
  TextArea,
  CloseButton,
  Footer,
  Line,
} from './styledComponents';
import { IPopupLabels } from '../../interfaces/dataStructure/ILabels';
import { IListItem } from '../../interfaces/dataStructure/IListItem';

import {
  ButtonDefault,
  AlertInfo,
} from '../../containers/RecipesList/styledComponents';

interface ICustomPopupProps {
  closePopupMethod: (data?: IListItem | any) => void;
  recipeNameValue?: string;
  recipeDetailsValue?: string;
  recipeIdNumber?: number;
  options: ICustomPopupOptions;
}

type ICustomPopupOptions = {
  buttonLabel: string;
  headerLabel: string;
  method: (data: IListItem) => void;
};

interface ICustomPopupState {
  [key: string]: string | boolean | number;
}

class CustomPopup extends PureComponent<ICustomPopupProps, ICustomPopupState> {
  public state = {
    recipeNameValue: this.props.recipeNameValue || '',
    recipeDetailsValue: this.props.recipeDetailsValue || '',
    submitError: false,
  };

  private handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    const { recipeNameValue, recipeDetailsValue } = this.state;

    if (!recipeNameValue.length || !recipeDetailsValue.length)
      return this.setState({ submitError: true });

    const data = {
      id: this.props.recipeIdNumber || getTimestampId(),
      name: recipeNameValue,
      details: recipeDetailsValue,
    };

    this.props.options.method(data);
  };

  private handleInput = (key: string) => (e: any) =>
    this.setState({ [key]: e.target.value });

  private closePopup = () => this.props.closePopupMethod();

  public render() {
    const { recipeNameValue, recipeDetailsValue, submitError } = this.state;
    const { headerLabel, buttonLabel } = this.props.options;
    const {
      alertInfo,
      closeButton,
      title,
      ingredients,
    }: IPopupLabels = labels.popup;

    // Re-render is happening here because we use here controlled component and this is how it should work.
    // If performance is major issue here we can consider here also to move Mask, Header, Labels and Footer to external pure components for avoiding re-render.
    return (
      <CustomPopupContainer>
        <Mask onClick={this.closePopup} />
        <Form onSubmit={this.handleSubmit}>
          <Header>
            {headerLabel}
            {this.props.recipeNameValue}
            <ButtonIconClose type="button" onClick={this.closePopup} />
          </Header>
          <FormBody>
            <Label width={'50%'}>
              <LabelTitle>{title}</LabelTitle>
              <InputText
                type="text"
                value={recipeNameValue}
                name="recipeName"
                onChange={this.handleInput('recipeNameValue')}
              />
              <Line />
            </Label>
            <Label width={'100%'}>
              <LabelTitle>{ingredients}</LabelTitle>
              <TextArea
                value={recipeDetailsValue}
                name="recipeDetails"
                placeholder="Separeted by comma's"
                onChange={this.handleInput('recipeDetailsValue')}
              />
              <Line />
            </Label>
          </FormBody>
          <Footer>
            {submitError && <AlertInfo>{alertInfo}</AlertInfo>}
            <ButtonDefault type="submit">{buttonLabel}</ButtonDefault>
            <CloseButton type="button" onClick={this.closePopup}>
              {closeButton}
            </CloseButton>
          </Footer>
        </Form>
      </CustomPopupContainer>
    );
  }
}

export default CustomPopup;
