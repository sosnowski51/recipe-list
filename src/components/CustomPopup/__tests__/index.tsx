import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import { ThemeProvider } from 'emotion-theming';
import theme from '../../../dresscode';
import CustomPopup from '../index';

const renderWithTheme = () => (
  <ThemeProvider theme={theme}>
    <CustomPopup
      closePopupMethod={() => 'test'}
      recipeNameValue={'Test item name'}
      recipeDetailsValue={'test details'}
      recipeIdNumber={1}
      options={{
        buttonLabel: 'Edit Recipe',
        headerLabel: 'Edit Recipe: ',
        method: () => 'test edit method',
      }}
    />
  </ThemeProvider>
);

it('renders custom popup component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(renderWithTheme(), div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create custom popup component snapshot', () => {
  const component = renderer.create(renderWithTheme());
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
