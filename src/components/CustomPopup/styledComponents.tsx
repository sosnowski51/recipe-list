import styled, { StyledComponent } from '@emotion/styled';
import { css } from '@emotion/core';

import { ITheme } from '../../interfaces/dresscode/ITheme';

type IThemeProps = {
  theme: ITheme;
};

export const CustomPopupContainer: StyledComponent<any, IThemeProps, any> = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Mask: StyledComponent<any, IThemeProps, any>  = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: ${({theme}) => theme.colors.black};
  opacity: 0.7;
  cursor: pointer;
`;

export const Form: StyledComponent<any, IThemeProps, any> = styled.form`
  z-index: 1;
  padding: 20px;
  box-sizing: border-box;
  background-color: ${({theme}) => theme.colors.white};
  border-radius: 3px;
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.3);
  width: 90%;
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    width: 70%;
    padding: 35px;
  }
  @media (min-width: ${({theme}) => theme.mediaQueries.desktop}) {
    width: 30%;
  }
`;

export const FormBody: StyledComponent<any, IThemeProps, any> = styled.div`
  padding: 15px;
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    padding: 30px;
  }
`;

export const Header = styled.div`
  display: flex;
  max-width: 100%;
  flex-basis: 100%;
  justify-content: space-between;
  font-weight: 100;
  font-size: 20px;
  line-height: 1;
  padding-bottom: 10px;
  margin-bottom: 10px;
  position: relative;
  &:after {
    position: absolute;
    content: '';
    width: 100%;
    height: 1px;
    left: 0;
    bottom: 0;
    background: linear-gradient(
      to right,
      rgba(214, 214, 214, 0) 0%,
      rgba(214, 214, 214, 1) 50%,
      rgba(214, 214, 214, 0) 100%
    );
  }
`;

export const ButtonIconClose: StyledComponent<any, IThemeProps, any>  = styled.button`
  width: 15px;
  height: 15px;
  border: 1px solid ${({theme}) => theme.colors.red};
  background-color: ${({theme}) => theme.colors.lightRed};
  border-radius: 100%;
  cursor: pointer;
  transition: background-color 0.3s ease-out;
  margin-top: -10px;
  margin-right: -10px;
  &:hover,
  &:focus {
    background-color: ${({theme}) => theme.colors.red};
  }
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    margin-top: -25px;
    margin-right: -25px;
  }
`;

export const Footer = styled.div`
  display: flex;
  flex-basis: 100%;
  max-width: 100%;
  flex-wrap: wrap;
  margin-top: 20px;
  justify-content: flex-start;
`;

export const Label = styled.label<any>`
  margin: 10px 0;
  display: inline-block;
  width: 100%;
  position: relative;
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    width: ${({ width }) => width};
  }
`;

export const LabelTitle = styled.div`
  font-size: 14px;
  font-weight: 100;
`;

export const Line: StyledComponent<any, IThemeProps, any>  = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 1px;
  width: 0;
  background-color: ${({theme}) => theme.colors.blue};
  transition: width 0.3s ease-out;
`;

const PlaceholderStyles = css`
  font-size: 14px;
  color: #eaeaea;
`;

const FieldsDefaultStyles = css`
  border-width: 0 0 1px 0;
  border-color: #eaeaea;
  padding: 10px 10px 5px;
  font-size: 18px;
  line-height: 1;
  box-sizing: border-box;
  width: 100%;
  &:focus {
    outline: 0;
    + div {
      width: 100%;
    }
  }
  &::-webkit-input-placeholder {
    ${PlaceholderStyles}
  }
  ::-moz-placeholder {
    ${PlaceholderStyles}
  }
  :-ms-input-placeholder {
    ${PlaceholderStyles}
  }
  :-moz-placeholder {
    ${PlaceholderStyles}
  }
`;

export const InputText = styled.input`
  ${FieldsDefaultStyles};
`;

export const TextArea = styled.textarea`
  ${FieldsDefaultStyles};
  height: 70px;
  max-width: 100%;
  min-width: 100%;
  line-height: 1.2;
  + div {
    bottom: 4px;
  }
`;

export const CloseButton: StyledComponent<any, IThemeProps, any>  = styled.button`
  border: 0;
  color: ${({theme}) => theme.colors.white};
  padding: 15px 25px;
  border-radius: 3px;
  text-transform: uppercase;
  font-weight: bold;
  line-height: 1;
  cursor: pointer;
  transition: background-color 0.3s ease-out;
  background-color: ${({theme}) => theme.colors.red};
  margin-left: 20px;
  &:hover,
  &:focus {
    background-color: ${({theme}) => theme.colors.lightRed};
  }
`;
