import React, { PureComponent } from 'react';

import { Title, Subtitle } from './styledComponents';

interface IListTitleProps {
  mainTitle: string;
  subtitle: string;
  description: string;
}

class ListTitle extends PureComponent<IListTitleProps> {
  public render() {
    const { mainTitle, subtitle, description } = this.props;

    return (
      <Title>
        {mainTitle}
        <Subtitle>
          {subtitle}
          <br />
          {description}
        </Subtitle>
      </Title>
    );
  }
}

export default ListTitle;
