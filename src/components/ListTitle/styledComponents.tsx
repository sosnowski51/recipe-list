import styled, { StyledComponent } from '@emotion/styled';

import { ITheme } from '../../interfaces/dresscode/ITheme';

type IThemeProps = {
  theme: ITheme;
};

export const Title: StyledComponent<any, IThemeProps, any> = styled.h1`
  color: ${({theme}) => theme.colors.white};
  font-size: 42px;
  line-height: 1;
  margin: 20px 0;
  flex-basis: 100%;
  max-width: 100%;
  text-align: center;
  font-weight: 100;
`;

export const Subtitle = styled.span`
  font-size: 20px;
  text-align: center;
  line-height: 1.2;
  font-weight: 100;
  display: block;
`;