import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import { ThemeProvider } from 'emotion-theming';
import theme from '../../../dresscode';
import ListTitle from '../index';

const renderWithTheme = () => (
  <ThemeProvider theme={theme}>
    <ListTitle
      mainTitle={'Test title'}
      subtitle={'Test subtitle'}
      description={'Test description'}
    />
  </ThemeProvider>
);

it('renders list title component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(renderWithTheme(), div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create list title component snapshot', () => {
  const component = renderer.create(renderWithTheme());
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
