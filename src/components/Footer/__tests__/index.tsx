import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import { ThemeProvider } from 'emotion-theming';
import theme from '../../../dresscode';
import Footer from '../index';

const renderWithTheme = () => (
  <ThemeProvider theme={theme}>
    <Footer>Test footer</Footer>
  </ThemeProvider>
);

it('renders footer component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(renderWithTheme(), div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create footer component snapshot', () => {
  const component = renderer.create(renderWithTheme());
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
