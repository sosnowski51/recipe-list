import React, { PureComponent } from 'react';

import { FooterContainer } from './styledComponents';

class Footer extends PureComponent<{}> {
  public render() {
    return (
      <FooterContainer>{this.props.children}</FooterContainer>
    );
  }
}

export default Footer;
