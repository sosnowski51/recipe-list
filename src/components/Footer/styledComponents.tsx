import styled, { StyledComponent } from '@emotion/styled';

import { ITheme } from '../../interfaces/dresscode/ITheme';

type IThemeProps = {
  theme: ITheme;
};

export const FooterContainer: StyledComponent<any, IThemeProps, any> = styled.div`
  margin-top: 20px;
  font-size: 18px;
  width: 100%;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
  text-align: right;
  color: ${({theme}) => theme.colors.silver};
  align-self: flex-end;
  padding: 10px 30px;
`;
