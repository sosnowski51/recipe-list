import styled, { StyledComponent } from '@emotion/styled';
import { css } from '@emotion/core';

import { ITheme } from '../../interfaces/dresscode/ITheme';

type IThemeProps = {
  theme: ITheme;
};

export const ListItem: StyledComponent<any, IThemeProps, any> = styled.li`
  padding: 0 20px;
  background-color: #fbfbfb;
  &:nth-of-type(odd) {
    background-color: ${({theme}) => theme.colors.lightWhite};
  }
  &:not(:last-child) {
    border-bottom: 4px solid ${({theme}) => theme.colors.white};
  }
  @media (min-width: ${({theme}) => theme.mediaQueries.mobile}) {
    padding: 0 30px;
  }
`;

export const ListItemTitle: StyledComponent<any, IThemeProps, any> = styled.div`
  cursor: pointer;
  padding: 20px 0 15px;
  display: flex;
  justify-content: space-between;
  transition: all 0.3s ease-out;
  position: relative;
  &:after {
    position: absolute;
    content: '';
    width: 0;
    height: 1px;
    transition: width 0.7s ease-out;
    bottom: 0;
    left: 0;
    background-color: ${({theme}) => theme.colors.blue};
  }
  &:hover {
    color: ${({theme}) => theme.colors.blue};
  }
  &.active {
    color: ${({theme}) => theme.colors.blue};
    font-weight: bold;
    &:after {
      width: 100%;
    }
    + .item-details {
      max-height: 500px;
    }
    span {
      opacity: 1;
    }
  }
`;

export const ListItemContent = styled.div`
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.3s cubic-bezier(0.5, 0.15, 0.5, 0.85);
`;

export const ListItemWrapper = styled.div`
  padding: 20px 0;
`;

export const ListItemDetails: StyledComponent<any, IThemeProps, any> = styled.span`
  font-weight: 100;
  font-size: 18px;
  line-height: 1;
  margin: 10px;
  background-color: ${({theme}) => theme.colors.white};
  border-radius: 3px;
  padding: 10px;
  display: inline-block;
  box-shadow: 7px 7px 10px 1px rgba(0, 0, 0, 0.07);
  cursor: pointer;
  transition: transform 0.3s ease-out;
  &.done {
    text-decoration: line-through;
    transform: scale(0.95);
  }
`;

export const ListItemOptions = styled.div`
  padding-top: 30px;
  text-align: right;
`;

export const Close: StyledComponent<any, IThemeProps, any> = styled.span`
  opacity: 0;
  transition: opacity 0.3s ease-out;
  font-size: 22px;
  line-height: 1;
  margin-top: -4px;
  margin-right: -10px;
  font-weight: 400;
  background: #ff3442;
  border-radius: 100%;
  width: 24px;
  height: 24px;
  text-align: center;
  color: ${({theme}) => theme.colors.white};
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.2);
`;

const ButtonsDefaultStyles = css`
  font-size: 16px;
  border: 0;
  color: #fff;
  padding: 7px 15px;
  border-radius: 3px;
  text-transform: uppercase;
  font-weight: 100;
  line-height: 1;
  cursor: pointer;
  transition: background-color 0.3s ease-out;
  &:focus {
    outline: 0;
  }
`;

export const DeleteButton: StyledComponent<any, IThemeProps, any> = styled.button`
  ${ButtonsDefaultStyles};
  background-color: ${({theme}) => theme.colors.lightRed};
  margin-left: 20px;
  &:hover,
  &:focus {
    background-color: ${({theme}) => theme.colors.red};
  }
`;

export const EditButton: StyledComponent<any, IThemeProps, any> = styled.button`
  ${ButtonsDefaultStyles};
  background-color: ${({theme}) => theme.colors.turquise};
  transition: background-color 0.3s ease-out;
  &:hover,
  &:focus {
    background-color: ${({theme}) => theme.colors.darkTurquise};
  }
`;
