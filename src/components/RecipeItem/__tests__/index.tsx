import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import { ThemeProvider } from 'emotion-theming';
import theme from '../../../dresscode';
import RecipeItem from '../index';

const mockedItem = {
  id: 1,
  name: 'test',
  details: 'test, test2',
};

const renderWithTheme = () => (
  <ThemeProvider theme={theme}>
    <RecipeItem
      item={mockedItem}
      key={mockedItem.id}
      showEditItemPopup={() => 'test'}
      deleteItem={() => 'test'}
    />
  </ThemeProvider>
);

it('renders recipe item component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(renderWithTheme(), div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create recipe item component snapshot', () => {
  const component = renderer.create(renderWithTheme());
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
