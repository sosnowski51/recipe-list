import React, { Component, SyntheticEvent } from 'react';

import * as labels from '../../const/labels.json';
import { IListItem } from '../../interfaces/dataStructure/IListItem';
import {
  ListItem,
  ListItemDetails,
  ListItemTitle,
  ListItemContent,
  ListItemOptions,
  ListItemWrapper,
  Close,
  DeleteButton,
  EditButton,
} from './styledComponents';
import { ILabels } from '../../interfaces/dataStructure/ILabels';

const handleItemClick = (className: string) => (e: SyntheticEvent) =>
  e.currentTarget.classList.toggle(className);

// I used index here as a key only because this list don't have unique ID keys.
// BTW if user will edit this component in UI, whole component will be re-rendered so it's doesn't metter to keys here will change
// I could use "item" as key, but we can't be 100% sure to user will not put the same item several times in UI.
const renderItemDetails = (details: string[]) =>
  details.map((item: string, index: number) => (
    <ListItemDetails key={index} onClick={handleItemClick('done')}>
      {item}
    </ListItemDetails>
  ));

interface IRecipeItemProps {
  item: IListItem;
  showEditItemPopup: (item: IListItem) => void;
  deleteItem: (id: number) => void;
}

class RecipeItem extends Component<IRecipeItemProps> {
  public shouldComponentUpdate(nextProps: any) {
    return (
      this.props.item.details !== nextProps.item.details ||
      this.props.item.name !== nextProps.item.name
    );
  }

  private handleEditButton = (item: IListItem) => () => this.props.showEditItemPopup(item);

  private handleDeleteButton = (id: number) => () => this.props.deleteItem(id);

  public render() {
    const { item } = this.props;
    const { editButtonLabel, deleteButtonLabel }: ILabels = labels;
    const splittedDetails = item.details.split(',');

    return (
      <ListItem>
        <ListItemTitle onClick={handleItemClick('active')}>
          {item.name} <Close>&times;</Close>
        </ListItemTitle>
        <ListItemContent className="item-details">
          <ListItemWrapper>
            {renderItemDetails(splittedDetails)}
            <ListItemOptions>
              <EditButton type="button" onClick={this.handleEditButton(item)}>
                {editButtonLabel}
              </EditButton>
              <DeleteButton
                type="button"
                onClick={this.handleDeleteButton(item.id)}
              >
                {deleteButtonLabel}
              </DeleteButton>
            </ListItemOptions>
          </ListItemWrapper>
        </ListItemContent>
      </ListItem>
    );
  }
}

export default RecipeItem;
