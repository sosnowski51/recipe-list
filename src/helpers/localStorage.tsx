export const localStorage = {
    setItem: (key: string, value: Object) => window.localStorage.setItem(key, JSON.stringify(value)),
    getItem: (key: string) => {
        const value: string | null = window.localStorage.getItem(key);
        return value && JSON.parse(value);
    },
}