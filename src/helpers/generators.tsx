const getTimestampId = () => new Date().getTime();

export { getTimestampId }; 