import { getTimestampId } from '../generators';

it('should return formatter number', () => {
  expect(getTimestampId()).toBeGreaterThan(100);
});