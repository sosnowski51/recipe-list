export interface IMediaQueries {
  mobile: string;
  desktop: string;
}