import { IColors } from './IColors';
import { IAnimations } from './IAnimations';
import { ITypography } from './ITypography';
import { IMediaQueries } from './IMediaQueries';

export interface ITheme {
  typography: ITypography;
  mediaQueries: IMediaQueries;
  colors: IColors;
  animations: IAnimations;
}
