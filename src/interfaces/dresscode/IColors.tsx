export interface IColors {
    black: string;
    white: string;
    lightWhite: string;
    silver: string;
    lightRed: string;
    lightPink: string;
    red: string;
    blue: string;
    turquise: string;
    darkTurquise: string;
}