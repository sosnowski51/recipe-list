export interface ILabels {
  mainTitle: string;
  subtitle: string;
  description: string;
  alertInfo: string;
  addNewButtonLabel: string;
  credentials: string;
  popup: IPopupLabels;
  editButtonLabel: string;
  deleteButtonLabel: string;
}

export interface IPopupLabels {
  alertInfo: string;
  closeButton: string;
  title: string;
  ingredients: string;
}
