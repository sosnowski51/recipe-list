export interface IListItem {
    id: number;
    details: string;
    name: string;
}